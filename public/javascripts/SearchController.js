/**
 * Created by Peter_Geschel on 22.10.2015.
 */

"use strict";

var app = angular.module('searchApp', []);

app.controller('SearchCtrl', function($scope, $location, $http, $sce) {

    $scope.hitcount = '-';

    $scope.searchButtonClicked = function(){

        var txt = $scope.searchtext

        $scope.searchtext = '';

        //execute only if text not empty
        if ( txt ) {
            var loadinggif = window.document.getElementById('loadingGif');
            loadinggif.style.display = '';
            $scope.hitcount = '';

            //prepare callbacks
            var successCallback = function(data){
                var j = angular.fromJson(data);

                //update display model
                loadinggif.style.display = 'none';
                document.getElementById("thesearchbutton").disabled = true;
                updateModel(j);

            };

            var errorCallback = function(data){

                var j = angular.fromJson(data);
                alert('errorCallback:'+j);
                loadinggif.style.display = 'none';
                $scope.hitcount = '-';
            };

            $http.get('/users/getDummyData').then(successCallback, errorCallback);
            //$http.get('http://libertarier.de/api/v1/search?term=staat').then(successCallback, errorCallback);
        } else {
            return;
        }

    };

    var updateModel = function(j) {
        $scope.hitcount = j.data.hits;
        $scope.chapters = j.data.chapters;
        for(var i=0; i<$scope.chapters.length; i++) {
            $scope.chapters[i].hit = i;
            $scope.chapters[i].displaytitle = $scope.chapters[i].book_title+', '+$scope.chapters[i].author+' - Kapitel '+ $scope.chapters[i].title;
            for(var j=0; j<$scope.chapters[i].highlights.length; j++) {
                $scope.chapters[i].highlighttext = $scope.chapters[i].highlighttext+$scope.chapters[i].highlights[j].text+' ';
            }
            $scope.chapters[i].highlighttext = $sce.trustAsHtml($scope.chapters[i].highlighttext);
            $scope.chapters[i].displaytext = $sce.trustAsHtml($scope.chapters[i].text);
        }
    }

    $scope.openChapterClicked = function(hitnumber) {

        for(var i=0; i<$scope.chapters.length; i++) {

            var openButton = window.document.getElementById('open_button_'+i);
            var closeButton = window.document.getElementById('close_button_'+i);
            var detailTextDiv = window.document.getElementById('detailed_text_div_'+i);

            if(i === hitnumber) {
                openButton.style.display = 'none';
                closeButton.style.display = '';
                detailTextDiv.style.display = '';
            }
            else {
                openButton.style.display = '';
                closeButton.style.display = 'none';
                detailTextDiv.style.display = 'none';
            }
        }
    }

    $scope.closeChapterClicked = function(hitnumber) {

        for(var i=0; i<$scope.chapters.length; i++) {
            var openButton = window.document.getElementById('open_button_'+i);
            var closeButton = window.document.getElementById('close_button_'+i);
            var detailTextDiv = window.document.getElementById('detailed_text_div_'+i);

            if(i === hitnumber) {

                openButton.style.display = '';
                closeButton.style.display = 'none';
                detailTextDiv.style.display = 'none';
            }
        }
    }

    $scope.searchInputChanged = function() {

        var button = document.getElementById("thesearchbutton");

        if($scope.searchtext) {
            button.disabled = false;
        }
        else {
            button.disabled = true;
        }
    }

});

