@echo off

if exist node_modules\.bin\jake goto :skip_building_npm
echo Building npm modules
call npm rebuild

:skip_building_npm

if exist app\node_modules goto :skip_installing_app_npm
echo installing app npm modules
call cd app && npm install

:skip_installing_app_npm

call node_modules\.bin\jake.cmd %*